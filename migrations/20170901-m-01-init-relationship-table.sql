BEGIN;

CREATE TABLE relationship (
  source text,
  target text,
  type text DEFAULT 'befriends',
  PRIMARY KEY (source, target, type),
  created_on timestamptz NOT NULL DEFAULT NOW()
);

COMMENT ON TABLE relationship IS 'Specifies one-way relationships (edges) of an arbitrary type from one node to another.';

CREATE INDEX ON relationship (type);

GRANT SELECT, INSERT, DELETE ON TABLE relationship TO spfm_api_service; -- Note: No update, do delete-and-reinsert for update-like functionality.

COMMIT;
