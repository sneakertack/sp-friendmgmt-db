BEGIN;

CREATE DOMAIN sha1_digest_hex AS varchar(40) CHECK (
  VALUE ~* '[0-9a-fA-F]{40}'
  AND LENGTH(VALUE) = 40
);

COMMENT ON DOMAIN sha1_digest_hex IS $$SHA1 digest stored as a 40-char hex string.$$;

CREATE TABLE schema_version (
  id integer PRIMARY KEY, -- Avoided serial, because want to ensure no holes.
  name text CONSTRAINT pgdb_history_filename CHECK (
    name ~* '^\d{8}-[a-z]-\d{2}(-.+)?$' -- e.g. 20140925-m-01 or 20140925-m-03-add-users.
    OR name = '%baseline'
  ),
  sha1sum sha1_digest_hex NOT NULL,
  installed_at timestamptz DEFAULT NOW()
);

COMMIT;
