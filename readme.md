Manages migrations of schema/DDL changes for the PostgreSQL database powering the SP Friend Management test app. It's a Node CLI tool that I wrote ~3 years ago, and it comes in useful all the time in my current/previous jobs, but the script itself is quite hacked together and I haven't really updated it much, so don't be too judgey on the code please.

![Screenshot of program comparing different database environments](screenshot.png)

# Prerequisites

- [Node & NPM](https://nodejs.org/) (tested on Node v4)
- one or more Postgres database instances that you would like to manage

# How to Use

0. Clone this repository.
0. Run `npm install`.
0. Create the database(s) and the `spfm_api_service` role on your database installation(s), e.g. with the following DDL executed by the `postgres` user:
  ```sql
  CREATE DATABASE spfriendmgmt_dev; -- Database can be any name.
  CREATE ROLE spfm_api_service PASSWORD 'spfm_api_service' NOSUPERUSER NOCREATEDB NOCREATEROLE LOGIN; -- Role name *must* be spfm_api_service. Skip if already created.
  GRANT CONNECT ON DATABASE spfriendmgmt_dev TO spfm_api_service;
  ```
0. Configure an `env.json` file. You can start out with `cp env.json.sample env.json`.
0. Try running `./migrate`; This will print out available commands and examples.  
   *(Do `chmod u+x migrate` to set executable permissions if necessary. Alternatively, run `node migrate`. From here on the command will just be referred to as `migrate`)*
0. `migrate [db] compare` gives you an overview.
0. `migrate [db[:env]] to latest` upgrades a database instance to the latest change.

# Upgrading Workflow

There is a forward directory and a backward directory. To update the database's schema, write your DDL in a .sql file and place it in the forward directory. Next write its counterpart that will undo all forward changes, and save it in the backward directory with the **same** .sql filename.

Filenames must be named like so:

```
yyyymmdd-x-nn-description.sql
```

- **yyyymmdd:** The date today, e.g. 20160831.
- **x:** An arbitrary character. As a default, put the 1st letter of your name. This will let you and your teammates know whose scripts go first when you both make changes on the same day.
- **nn:** 2 numbers (useful for if you are applying 3 scripts in a day. Use 01 in the morning, 02 in the afternoon, and 03 at night.)
- **description:** A dash-seperated description of the upgrade.

Examples:
```
20140927-m-01-reference-countries.sql
20140927-m-02-create-users.sql
```

Filename ordering then determines the order in which scripts are applied.

Further Guidelines:

0. *Wrap your migration in a transaction.* In your forward and backward scripts, start with a `BEGIN;` statement, and end with a `COMMIT;` statement.
0. *When `CREATE`ing something, don't add on `OR REPLACE`.* The prior state of whether the thing exists should already be known + consistent across environments. Do a `DROP` statement if an older version already exists, to make the migration's intention less ambiguous.
0. *Don't rewrite history.* Once an SQL script file and its corresponding backward script file have been committed, made public, and applied, it should be treated as public history, and no longer be amended. If you wish to revert a previously made change, create a new SQL script file dated today that counteracts the upgrade in question.
0. *Start your SQL script file with today's date,* unless you have a really good reason otherwise. Backdating is dangerous because in order to maintain schema consistency, the database has to roll back all scripts until it reaches the backdated script, apply the backdated script, then re-apply all later scripts.

# Configuration

A breakdown of the options settable in `env.json`:

```js
{

  spfriendmgmt: { // 1st-level keys differentiate databases (for this app, there's only 1 meaningful database).

    default: true, // Optional. Selects this database if none specified.
    histories: {

      some_other_history: {...}, // keys differentiate histories - though multiple histories are usually not needed.
      local: {

        default: true, // This history will be the default one.
        forwardDir: "./migrations", // Look for forward/up .sql files in this folder.
        backwardDir: "./migrations/backward", // Look for backward/down .sql files in this folder.
        shortcode: "migs/" // A 3-4 char column label for when you do `migrate compare`.
      }

    },
    environments: {

      development: { // keys differentiate environments.
        default: true, // Optional. Selects this environment if none specified.
        uri: "postgres://postgres:password@localhost:5432/pcsp", // Location of your database instance.
        shortcode: "dev", // A 3-4 char column label for when you do `migrate compare`.
        strict: false, // Optional - Won't bother you with confirmations when you're doing potentially destructive backward migrations, suitable for development. (default true).
        frontbackfront: true // When upgrading to the next version, instead of just running forward, we will 1) run forward, 2) run backward, then 3) run forward again. This is an easy heuristic to test whether your backward file was (probably) correctly written. Good for development. (default false).
      },
      staging: {...},
      production: {...},
    }
  },

  some_other_database: {...}

}
```

# Potential improvements

Implementable only if there turns out to be a need.

- Choosing of a history folder other than the default one.
- Not showing all entries in `migrate compare`, when the history gets too long.
