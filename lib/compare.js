'use strict';

var assert = require('assert');
var co = require('co');
var fs = require('fs');
var pg = require('pg');
var _ = require('lodash');
var checksum = require('./checksum');

module.exports = compare;

function compare(database) {
  co(function *() {
    console.log('histories: %d, environments: %d.', _.keys(database.histories).length, _.keys(database.environments).length);

    // Get history from history folders.
    var historyCollection = [];
    _.forOwn(database.histories, function (history, name) {
      var shortcode = history.shortcode || name.substring(0, 5);
      historyCollection.push({
        name: name,
        shortcode: shortcode,
        type: 'history',
        listThunk: function (callback) { // unfired function, fires later.
          fs.readdir(history.forwardDir, function (err, result) {
            if (err) return callback(err);
            result = result.filter(historyFilenamePattern).map(function (filename) {return /(.*)\.sql/.exec(filename)[1];}).sort();
            result = result.map(function (result) {
              return {
                name: result,
                hash: checksum.hash(fs.readFileSync(history.forwardDir+'/'+result+'.sql', 'utf8'))
              }
            });
            console.log('Loaded history: '+name+' ('+shortcode+')');
            return callback(null, result);
          })
        }
      });
    });

    // Get history from environment version_schema tables.
    var environmentCollection = [];
    _.forOwn(database.environments, function (environment, name) {
      var shortcode = environment.shortcode || name.substring(0, 5);
      environmentCollection.push({
        name: name,
        shortcode: shortcode,
        type: 'environment',
        listThunk: function (callback) {
          // Single-client Postgres connection.
          var client = new pg.Client(environment.uri);
          client.connect(function(err) {
            if (err) return callback(err);
            client.query('SELECT name, sha1sum FROM schema_version ORDER BY id ASC;', function(err, result) {
              if (err) {
                // We'll handle it if schema_version doesn't exist.
                if (err.code !== '42P01') return callback(err);

                client.end();
                console.log('Unmanaged environment:'.bgYellow.bold+' '+name+' ('+shortcode+')');
                return callback(null, []);
              }

              result = result.rows.map(function (x) {return {
                name: x.name,
                hash: x.sha1sum
              };})
              client.end();
              console.log('Loaded environment: '+name+' ('+shortcode+')');
              return callback(null, result);
            });
          });
        }
      });
    });

    var candidates = historyCollection.concat(environmentCollection);

    // All candidates should have a unique shortcode.
    assert((function () {
      var total = candidates.map(function (candidate) {return candidate.shortcode});
      var unique = _.uniq(total);
      return total.length === unique.length;
    })(), 'Comparison candidates must have unique shortcodes.');

    // Fire off the actual loading thunks that will retrieve each candidate's version history.
    try {
      var candidateLists = yield candidates.map(function (candidate) {return candidate.listThunk});
    } catch (e) {
      console.log('Error'.toUpperCase().bgRed.bold+' The following error occured while trying to connect to a environment / read a directory. At least 1 history or environment is not set up properly in env.json.\n');
      throw e;
    }
    for (let i = 0; i < candidates.length; i++) {
      candidates[i].list = candidateLists[i];
      delete candidates[i].listThunk;
    }

    // Now an inversion. Get the consolidated list of all versions existing in at least one candidate.
    var versionList = _.union.apply(_, candidates.map(function (candidate) {
      return candidate.list.map(function (x) {return x.name;});
    })).sort();

    // Data wrangling is done, let's pass this data to stdout renderer.
    console.log(); // newline seperator.
    require('./compare-stdout-render')({
      candidates: candidates,
      versions: versionList
    });
  })();
}

/**
 * HELPER FUNCTIONS
 */

function historyFilenamePattern(filename) {
  if (!/\.sql$/i.test(filename)) return false; // Throw away non-sql files.
  if (/^\d{8}-[a-z]-\d{2}(-.+)?\.sql$/i.test(filename)) return true; // 20140925-m-02-something.sql is allowed.
  if (filename === '%baseline.sql') return true; // 'The first baseline script file is allowed.'
  throw new Error('SQL migration script filename doesn\'t follow expected pattern: '+filename);
}
