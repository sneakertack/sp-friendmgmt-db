'use strict';

var assert = require('assert');
var _ = require('lodash');

// Load local config file.
try {
  var config = require('../env.json');
} catch (e) {
  if (e.code === 'MODULE_NOT_FOUND') {
    console.log('ERROR: You need a local env.json file. Please create env.json using env.json.sample as a reference.');
    process.exit(1);
  }
  throw e;
}

// Sanity checking, make sure file is alright.
_.forOwn(config, function (database, name) {
  assert(['histories', 'environments'].every(function (key) {return _.isPlainObject(database[key]);}), name+ ' database should have the \'histories\' and \'environments\' config objects defined.');
  _.forOwn(database.histories, function (history, name) {
    assert(_.isString(history.forwardDir), ''+name+' history should have a forwardDir property.');
    assert(_.isString(history.backwardDir), ''+name+' history should have a backwardDir property.');
  });
  _.forOwn(database.environments, function (env, name) {
    assert(_.isString(env.uri), ''+name+' environment should have a uri property.');
  });
});


module.exports = config;
