'use strict';

var colors = require('colors');
var _ = require('lodash');

module.exports = render;

function render(data) {
  // Render headers.
  var candidateColumns = data.candidates.map(function (candidate) {
    return {
      shortcode: candidate.shortcode,
      versions: candidate.list,
      width: candidate.shortcode.length // Used for calculating how many spaces to render.
    };
  });
  candidateColumns.forEach(function (column) {
    process.stdout.write(column.shortcode.bold.underline + ' ');
  });
  process.stdout.write('Version'.toUpperCase().bold.inverse+'\n');

  // Render body.
  var allRowsMatching = true;
  var allHashesMatching = true;
  _.forEachRight(data.versions, function (version) {
    var outputLine = '';
    let allMatching = candidateColumns.every(function (candidate) {
      return candidate.versions.map(function (x) {return x.name;}).indexOf(version) > -1;
    });
    if (allMatching === false) allRowsMatching = false;
    let rowHashes = _.compact(_.union(candidateColumns.map(function (candidate) {
      var match = _.find(candidate.versions, function (candidateVersion) {
        return candidateVersion.name === version;
      });
      if (match) return match.hash;
      return undefined;
    })));
    if (rowHashes.length > 1) allHashesMatching = false;
    candidateColumns.forEach(function (candidate) {
      // Left padding.
      outputLine += Array(1 + Math.floor((candidate.width - 1) / 2)).join(' ')
      // Status symbol.
      if (candidate.versions.map(function (version) {return version.name}).indexOf(version) > -1) {
        if (allMatching) {
          outputLine += '✔'.green;
        } else {
          outputLine += '•'.yellow;
        }
      }  else {
        outputLine += ' ';
      }
      // Right padding.
      outputLine += Array(1 + Math.ceil((candidate.width - 1) / 2)).join(' ');
      // Right margin.
      outputLine += ' ';
    });
    outputLine += version[allMatching ? 'green' : 'yellow'].dim+' ';
    outputLine += rowHashes.map(function (hash) {return '#'+hash.substring(0, 7);}).join(', ').gray;
    if (rowHashes.length > 1) {
      outputLine = outputLine.bgRed;
    }
    outputLine += '\n';
    process.stdout.write(outputLine);
  });

  console.log();
  if (allRowsMatching && allHashesMatching) {
    console.log('All environments are up to date.');
  } else {
    console.log('Not all environments are up to date.');
  }
  if (!allHashesMatching) {
    console.log();
    console.log('Checksum Mismatch'.toUpperCase().bgRed.bold+' Conflict between source and applied: The checksums of the version(s) highlighted in red do not validate against each other. Please resolve this historical conflict.');
  }
}
