'use strict';

var fs = require('fs');
var pg = require('pg');
var co = require('co');
var readlineSync = require('readline-sync');
var _ = require('lodash');
var thunkify = require('thunkify');
var colors = require('colors');
var checksum = require('./checksum');

module.exports = to;

function to(environment, history, commandChain) {
  co(function *() {

    // Check that version is specified.
    if ((commandChain || []).length === 0) {
      console.log('Rejection:'.bgRed.bold+' You must specify a version to migrate to (specify \'to latest\', if you\'re lazy).');
      process.exit(1);
    }

    // Read source directories
    var forwardDir = history.forwardDir;
    var forwardList = fs.readdirSync(forwardDir).filter(historyFilenamePattern).map(function (filename) {return /(.*)\.sql/.exec(filename)[1];}).sort();

    var backwardDir = history.backwardDir;
    var backwardList = fs.readdirSync(backwardDir).filter(historyFilenamePattern).map(function (filename) {return /(.*)\.sql/.exec(filename)[1];}).sort();

    // Display warnings if user has a forward file without a complementary backward one. This may be running in n-square timing, but it probably doesn't matter.
    forwardList.forEach(function (version) {
      if (!(backwardList.indexOf(version) > -1)) {
        console.log('Warn:'.toUpperCase().yellow+' Could not locate backward script file for %s.sql.'.gray, version);
      }
    });

    var targetVersion = commandChain.shift() // TODO: Allow passing in the hash instead of the version name.
    switch (targetVersion) {
      case 'latest':
        targetVersion = forwardList[forwardList.length - 1];
        break;
      case '2nd':
        targetVersion = forwardList[forwardList.length - 2];
        break;
    }
    if (!targetVersion || !_.includes(forwardList, targetVersion)) {
      console.log('Error:'.toUpperCase().bold.bgRed+' Unknown version specified: %s', targetVersion);
      process.exit(1);
    }

    // Trim off every version after the target in the forward list.
    forwardList = _.take(forwardList, forwardList.indexOf(targetVersion) + 1);

    console.log('Target version: %s', targetVersion.bold);
    console.log('Retrieving database version history...');

    // Start the database connection.
    try {
      var client = new pg.Client(environment.uri);
      yield client.connect.bind(client);
      var query = thunkify(client.query.bind(client));
    } catch (e) {
      console.log('Error'.toUpperCase().bgRed.bold+' The following error occured while trying to connect to the database. Perhaps check your env.json?\n');
      throw e;
    }


    // Access the DB's meta version memory, to see what's the DB's actual state.
    var dbHistory;
    let retry = true;
    while (retry) {
      try {
        var dbHistoryResult = yield query('SELECT name FROM schema_version ORDER BY id ASC;');
        dbHistory = dbHistoryResult.rows.map(function (x) {return x.name;}).sort();
        console.log('Current database version: %s', dbHistory[dbHistory.length - 1].bold);
        retry = false;
      } catch (e) {
        // No meta table found? It may be a new database. Verify that this db has no tables, then start with %baseline.sql.
        // Rethrow if failure due to some other reason.
        if (e.message !== 'relation "schema_version" does not exist') throw e;

        console.log('Unable to find table \'schema_version\'. Checking to see if database is empty...');

        var publicTablesResult = yield query('SELECT * FROM pg_catalog.pg_tables WHERE schemaname=\'public\';');
        if (publicTablesResult.rows.length !== 0) {
          publicTablesResult.rows.forEach(function (table) {
            console.log('Found public table '+table.tablename+'.');
          })
          console.log('Warn:'.toUpperCase().yellow+' Database appears to have pre-existing data (db has no public "schema_version" table, yet has existing public tables). ');
        }

        if (readlineSync.question('This looks like a new database. Type y to set up version tracking. (y/n) ').toLowerCase() !== 'y') {
          console.log('Aborted.');
          process.exit(0);
        }

        console.log('Starting setup for new database...')

        var payload = loadSql(forwardDir+'/%baseline.sql');
        var hash = checksum.hash(payload);
        var baselineResult = yield query(payload);
        console.log('Table schema_version successfully created.')
        yield query('INSERT INTO schema_version (id, name, sha1sum) VALUES (1, $1, $2)', ['%baseline', hash]);
        console.log('Populated first baseline entry in table schema_version. Database is now set up for version tracking.')
      }
      // Goes back up and tries again.
    }

    // Compares both histories from the beginning, taking them out if they both match. The remainder represents the diff between both histories.
    var highestMatchingIndex = -1;
    while ((highestMatchingIndex + 1) < Math.max(forwardList.length, dbHistory.length) && forwardList[highestMatchingIndex + 1] === dbHistory[highestMatchingIndex + 1]) {
      highestMatchingIndex++;
    }

    if (highestMatchingIndex === Math.max(forwardList.length, dbHistory.length) - 1) {
      console.log('OK'.bgGreen.bold+' Database version is already up to date.');
      process.exit(0);
    }

    console.log('Planning script application sequence...');
    var sequence = [];
    for (let i = dbHistory.length - 1; i > highestMatchingIndex; i--) {
      sequence.push({
        name: dbHistory[i],
        type: 'backward'
      });
      console.log(('%d: %s '+'<<'.bold+' %s').red, sequence.length, dbHistory[i - 1], dbHistory[i]);
    }
    for (let i = highestMatchingIndex + 1; i < forwardList.length; i++) {
      sequence.push({
        name: forwardList[i],
        type: 'forward'
      });
      let isFbf = environment.frontbackfront ? ' (x2)' : '';
      console.log(('%d: %s '+'>>'.bold+' %s'+isFbf).blue, sequence.length, forwardList[i - 1], forwardList[i]);
    }

    if (environment.strict !== false) {
      if (readlineSync.question('About to run the sequence listed above. Confirm? (y/n) ').toLowerCase() !== 'y') {
        console.log('Aborted.');
        process.exit(0);
      }
      if (sequence.some(function (item) {return item.type === 'backward'})) {
        console.log();
        if (readlineSync.question('*SEVERE WARNING*'.bgRed.bold+(' The sequence above includes schema rollbacks, which can '+'irreversibly remove accumulated row data within dropped tables/columns.'.bold+' Are you absolutely sure you want to run the migration sequence? (y/n) ').red).toLowerCase() !== 'y') {
          console.log('Aborted.');
          process.exit(0);
        }
        console.log();
        if (readlineSync.question('Please enter \'y\' 1 more time: '.magenta.dim).toLowerCase() !== 'y') {
          console.log('Aborted.');
          process.exit(0);
        }
        console.log();
      }
    }

    process.stdout.write('0'.gray);
    for (let i = 0; i < sequence.length; i++) {
      if (sequence[i].type === 'backward') {
        yield query(loadSql(backwardDir+'/'+sequence[i].name+'.sql'));
        yield query('DELETE FROM schema_version WHERE name=$1', [sequence[i].name]);
        process.stdout.write((i+1).toString().bgGreen);
      } else if (sequence[i].type === 'forward') {
        let payload = loadSql(forwardDir+'/'+sequence[i].name+'.sql');
        let hash = checksum.hash(payload);
        yield query(payload);
        yield query('INSERT INTO schema_version (id, name, sha1sum) VALUES ((SELECT MAX(id) FROM schema_version) + 1, $1, $2)', [sequence[i].name, hash]);
        process.stdout.write((i+1).toString().bgGreen);

        // Do front-back-front confirmation, if enabled for this environmentment.
        if (environment.frontbackfront === true) {
          yield query(loadSql(backwardDir+'/'+sequence[i].name+'.sql'));
          yield query('DELETE FROM schema_version WHERE name=$1', [sequence[i].name]);
          let payload = loadSql(forwardDir+'/'+sequence[i].name+'.sql');
          let hash = checksum.hash(payload);
          yield query(payload);
          yield query('INSERT INTO schema_version (id, name, sha1sum) VALUES ((SELECT MAX(id) FROM schema_version) + 1, $1, $2)', [sequence[i].name, hash]);
          process.stdout.write('+'.bgGreen);
        }

      } else {throw new Error('Unknown sequence operation type (expected forward or backward)');}
    }
    process.stdout.write(' Completed!'.green+'\n');
    console.log();
    console.log('Database successfully migrated to version %s.', targetVersion.bold);
    process.exit();
  })();
}



/**
 * HELPER FUNCTIONS
 */

function historyFilenamePattern(filename) {
  if (!/\.sql$/i.test(filename)) return false; // Throw away non-sql files.
  if (/^\d{8}-[a-z]-\d{2}(-.+)?\.sql$/i.test(filename)) return true; // 20140925-m-02-something.sql is allowed.
  if (filename === '%baseline.sql') return true; // 'The first baseline script file is allowed.'
  throw new Error('SQL migration script filename doesn\'t follow expected pattern: '+filename);
}

function loadSql(filepath) {
  var contents = fs.readFileSync(filepath, {encoding: 'utf8'});

  // Check for an SQL transaction start.
  var lines = contents.split('\n');
  lines.some(function (line) {
    if (/^--/.test(line) || line.trim().length === 0) return false; // Ignore single-line comments and empty lines. DEBT: Currently does not account for multi-line comments.
    if (/^ *BEGIN *;/i.test(line)) return true;
    throw new Error(''+filepath+' failed transaction guard check, does not appear to begin with a BEGIN; statement.');
  });

  // Check for an SQL transaction end, from the back.
  lines.reverse();
  lines.some(function (line) {
    if (/^--/.test(line) || line.trim().length === 0) return false; // Ignore single-line comments and empty lines. DEBT: Currently does not account for multi-line comments.
    if (/^ *COMMIT *;/i.test(line)) return true;
    throw new Error(''+filepath+' failed transaction guard check, does not appear to end with a COMMIT; statement.');
  });

  return contents;
}
