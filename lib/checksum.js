'use strict';

var crypto = require('crypto');

module.exports = {
  hash: sha1sum
};

function sha1sum(data) {
  var sha1 = crypto.createHash('sha1');

  // Strip newlines, to work around platform inconsistencies.
  data = data.replace(/\r/g, '').replace(/\n/g, '');
  sha1.update(data, 'utf8');

  return sha1.digest('hex');
}
